<?php

namespace Tests\Unit;

use Mvochoa\APIDoc\GenerateClass;
use Tests\TestCase;

/**
 * Description.
 *
 * @path-doc /api/doc/one
 * @method-doc PUT
 * @title-doc Title documentation
 * @description-doc Description documentation and
 *                  more documentation\n
 *                  finish description
 * @param-doc {String|required} field1 Description field 1
 * @param-doc {Number} field2 Description field 2
 *                      more description field 2
 * @param-doc {Number|required|3.1416} field3 Description field 3
 */
class GenerateTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->doc = new GenerateClass(__CLASS__);
    }

    /**
     * Description.
     */
    public function testBasicTestWIthTitleAndDescription()
    {
        $example = $this->doc->setExampleMethod(__CLASS__, __FUNCTION__, [
            'field1' => 'Hola Mundo',
            'field2' => 1,
        ]);
        $this->assertEquals($this->doc->getURL(), '/api/doc/one');

        $example->setResponse([
            'status' => true,
            'data'=> '/hello/world',
        ], 200);
    }

    /**
     * Description.
     *
     * @title-doc Title method documentation
     * @description-doc Description to method
     *                  the documentation.
     */
    public function testBasicTest()
    {
        $example = $this->doc->setExampleMethod(__CLASS__, __FUNCTION__, [
            'field1' => 'Hola Mundo',
        ]);
        $this->assertEquals($this->doc->getURL(), '/api/doc/one');

        $example->setResponse([
            'status' => true,
            'data'=> 'Success with utf8 á é í ó ú ñ',
        ], 200);
    }

    /**
     * Description.
     *
     * @title-doc Title method documentation
     * @description-doc No params.
     */
    public function testBasicTestNoParams()
    {
        $example = $this->doc->setExampleMethod(__CLASS__, __FUNCTION__);
        $this->assertEquals($this->doc->getURL(), '/api/doc/one');

        $example->setResponse([
            'status' => true,
            'data'=> 'Success',
        ], 200);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if ($this->doc) {
            $this->doc->publish();
        }
    }
}

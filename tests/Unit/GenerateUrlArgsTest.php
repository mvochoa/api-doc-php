<?php

namespace Tests\Unit;

use Mvochoa\APIDoc\GenerateClass;
use Tests\TestCase;

/**
 * Description.
 *
 * @path-doc /api/doc/args/{one}/other/{two}
 * @method-doc DELETE
 * @title-doc Title documentation 2
 * @description-doc Description documentation
 * @param-doc {Number|required|1} three Argument three
 * @param-doc {Number|required|2} four Argument for
 */
class GenerateUrlArgsTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->doc = new GenerateClass(__CLASS__);
    }

    /**
     * Description.
     */
    public function testBasicTestWIthTitleAndDescription()
    {
        $example = $this->doc->setExampleMethod(__CLASS__, __FUNCTION__);
        $this->assertEquals($example->getURL([
            'one' => 1,
            'two' => 2,
        ]), '/api/doc/args/1/other/2');

        $example->setResponse([
            'status' => true,
            'data'=> '/hello/world',
        ], 200);
    }

    /**
     * Description.
     *
     * @title-doc Title method documentation
     * @description-doc Description to method
     *                  the documentation.
     */
    public function testBasicTest()
    {
        $example = $this->doc->setExampleMethod(__CLASS__, __FUNCTION__);
        $this->assertEquals($this->doc->getURL(), '/api/doc/args/{one}/other/{two}');
        $example->getParamsDefault();

        $example->setResponse([
            'status' => true,
            'data'=> 'Success with utf8 á é í ó ú ñ',
        ], 200);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if ($this->doc) {
            $this->doc->publish();
        }
    }
}

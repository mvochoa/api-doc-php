<?php

namespace Mvochoa\APIDoc;

use Exception;
use ReflectionClass;

class GenerateClass extends Generate
{
    public function __construct($class)
    {
        $rc = new ReflectionClass($class);
        $comments = Utils::parseComment($rc->getDocComment());

        foreach (['@path-doc', '@title-doc'] as $key) {
            if (! array_key_exists($key, $comments)) {
                throw new Exception('The attribute '.$key.' not found');
            }
        }

        $url = trim($comments['@path-doc']);
        $title = trim($comments['@title-doc']);
        $method = Generate::POST;

        if (array_key_exists('@method-doc', $comments)) {
            $method = trim($comments['@method-doc']);
        }

        parent::__construct($title, $url, $method);
        $this->setKey($rc->getName());
        if (array_key_exists('@param-doc', $comments)) {
            $params = $comments['@param-doc'];
            if (! is_array($params)) {
                $params = [$params];
            }

            foreach ($params as $param) {
                $this->addParam(Utils::parseParamComment($param));
            }
        }
        if (array_key_exists('@description-doc', $comments)) {
            $this->setDescription(trim($comments['@description-doc']));
        }
    }
}

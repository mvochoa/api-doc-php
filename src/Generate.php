<?php

namespace Mvochoa\APIDoc;

use JsonSerializable;

class Generate implements JsonSerializable
{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';

    private $key;
    private $title;
    private $url;
    private $method;
    private $params;
    private $keyParams;
    private $description;
    private $example;

    public function __construct($title, $url, $method = Generate::POST)
    {
        if (array_search($method, [self::GET, self::POST, self::PUT, self::DELETE]) == -1) {
            throw new Exception('The method is incorrect');
        }

        $this->title = $title;
        $this->url = $url;
        $this->method = $method;
        $this->description = '';
        $this->keyParams = [];
        $this->params = [];
    }

    public function setKey($key)
    {
        $this->key = $key;
    }

    public function getKey()
    {
        $key = $this->key;
        if (! $key) {
            $key = $this->title;
        }

        return $key;
    }

    public function getURL($args = [])
    {
        $url = $this->url;
        foreach ($args as $key => $value) {
            $url = preg_replace("/\{$key\}/i", $value, $url);
        }

        return $url;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function addParam(Param $param)
    {
        array_push($this->params, $param);
        $this->keyParams[$param->getName()] = $param->getValue();
    }

    public function setExample($params = [])
    {
        $this->example = new Example();
        $this->example->setKeyParams($this->keyParams);
        $this->example->setParams($params);

        return $this->example;
    }

    public function setExampleMethod($class, $method, $params = [])
    {
        $this->example = new ExampleMethod($class, $method);
        $this->example->setUrl($this->url);
        $this->example->setKeyParams($this->keyParams);
        $this->example->setParams($params);

        return $this->example;
    }

    public function publish()
    {
        $keyExample = null;
        $key = $this->getKey();
        $settings = $this->getSettings();
        $cache = $this->getCache($settings['file_cache']);
        $index = $this->setKeyCache($cache, $this->url, $key);
        $dir = $this->createDirectory($settings['directory'], $this->url, $this->method);
        $cache[$index][$key] = array_merge($cache[$index][$key], $this->jsonSerialize(), [
            'directory' => preg_replace('/\/{2,}/i', '/', str_replace($settings['directory'], '/', $dir)),
            'link_with_md_extension' => $settings['link_with_md_extension'],
        ]);

        if ($this->example) {
            $pathExample = $this->parsePath($dir, $this->example->getKey());
            $cache[$index][$key]['examples'][basename($pathExample)] = $this->example->getTitle();
            $this->example->writeExample($pathExample, $cache[$index][$key]);
        }

        $tmp = new Template(Template::DOC, $cache[$index][$key]);
        file_put_contents($dir.'Home.md', $tmp->getOutput());

        $tmp = new Template(Template::HOME, ['data' => $cache, 'titles' => $settings['titles'], 'link_with_md_extension' => $settings['link_with_md_extension']]);
        if (array_key_exists('home', $settings) && array_key_exists('template', $settings['home'])) {
            $tmp->setOutput(preg_replace('/{{\s*[Ll]inks\s*}}/i', $tmp->getOutput(), file_get_contents($settings['home']['template'])));
        }

        file_put_contents($settings['directory'].'/Home.md', $tmp->getOutput());
        $this->saveCache($settings['file_cache'], $cache);
    }

    private function getSettings()
    {
        $settings = [
            'directory' => 'doc',
            'file_cache' => 'doc.json',
            'link_with_md_extension' => true,
            'titles' => [],
        ];
        if (file_exists('./.php_doc')) {
            $fileSettings = require './.php_doc';
            if (array_key_exists('link_with_md_extension', $fileSettings)) {
                $settings['link_with_md_extension'] = $fileSettings['link_with_md_extension'];
                unset($fileSettings['link_with_md_extension']);
            }
            $settings = array_merge_recursive($settings, $fileSettings);
        }

        return $settings;
    }

    private function getCache($file)
    {
        $cache = [];

        try {
            if (file_exists($file)) {
                $cache = json_decode(file_get_contents($file), true);
            }
        } catch (\Throwable $th) {
        }

        return $cache;
    }

    private function createDirectory($dir, $url, $method)
    {
        $dir .= '/'.preg_replace('/[{}]/i', '_', $url).'/'.$method.'/';

        $dir = preg_replace('/\/{2,}/i', '/', $dir);
        if (! file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        return $dir;
    }

    private function saveCache($file, $cache)
    {
        try {
            file_put_contents($file, json_encode($cache, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        } catch (\Throwable $th) {
        }
    }

    private function setKeyCache(&$cache, $url, $key)
    {
        $url = preg_replace('/\/{2,}/i', '/', preg_replace('/{[a-zA-Z0-9-_]+}(\/)?/i', '/', $url));
        if (! array_key_exists($url, $cache)) {
            $cache[$url] = [];
        }

        if (! array_key_exists($key, $cache[$url])) {
            $cache[$url][$key] = [
                'examples' => [],
            ];
        }

        return $url;
    }

    private function parsePath($path, $name = null)
    {
        $path = preg_replace('/{[a-zA-Z0-9-_]+}(\/)?/i', '/', $path);
        if ($name) {
            $name = str_replace(' ', '', $name);
            $name = str_replace('\\', '', $name);
            $name = preg_replace('/([Tt]est(s)?|[Uu]nit(s)?|[Ff]eature(s)?)/i', '', $name);
            $path .= '/'.$name;
        }

        return str_replace('//', '/', $path);
    }

    public function jsonSerialize()
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'url' => $this->url,
            'method' => $this->method,
            'params' => $this->params,
        ];
    }
}

<?php

namespace Mvochoa\APIDoc;

class Template
{
    const HOME = __DIR__.'/templates/Home.md.php';
    const DOC = __DIR__.'/templates/Doc.md.php';
    const EXAMPLE = __DIR__.'/templates/Example.md.php';

    private $output;

    public function __construct($file, $args)
    {
        if (! file_exists($file)) {
            return '';
        }

        if (is_array($args)) {
            extract($args);
        }

        ob_start();
        include $file;

        $this->output = ob_get_clean();
    }

    public function getOutput()
    {
        return $this->output;
    }

    public function setOutput($output)
    {
        $this->output = $output;
    }
}

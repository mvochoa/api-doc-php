<?php

namespace Mvochoa\APIDoc;

use Exception;
use JsonSerializable;

class Example implements JsonSerializable
{
    private $key;
    private $title;
    private $response;
    private $status;
    private $description;
    private $params;
    private $keyParams;
    private $url;
    private $args;

    public function __construct()
    {
        $this->args = [];
        $this->params = [];
        $this->keyParams = [];
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setUrl($url)
    {
        $this->args = [];
        $this->url = $url;
        $pattern = '#{([a-zA-Z0-9-_]+)}#';
        preg_match_all($pattern, $this->url, $matches, PREG_PATTERN_ORDER);
        if (count($matches) > 1) {
            foreach ($matches[1] as $key => $value) {
                $this->args[$value] = '';
            }
        }
    }

    public function getURL($args = [])
    {
        $url = $this->url;
        foreach ($args as $key => $value) {
            if (! array_key_exists($key, $this->args)) {
                throw new Exception('The arg '.$key.' not exists');
            }
            $url = preg_replace("/\{$key\}/i", $value, $url);
        }

        $this->setArgs($args);

        return $url;
    }

    public function setArgs($args)
    {
        $this->args = $args;
    }

    public function setKey($key)
    {
        $this->key = $key;
    }

    public function getKey()
    {
        if ($this->key) {
            return $this->key;
        }

        return $this->title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setResponse($response, $status)
    {
        $this->response = $response;
        $this->status = $status;
    }

    public function setKeyParams($keys)
    {
        $this->keyParams = $keys;
    }

    public function setParams($params)
    {
        foreach ($params as $key => $value) {
            if (! array_key_exists($key, $this->keyParams)) {
                throw new Exception('The param '.$key.' not exists');
            }
            $this->params[$key] = $value;
        }

        return $this->params;
    }

    public function getParams()
    {
        $params = [];
        foreach ($this->params as $key => $value) {
            if (! is_null($value)) {
                $params[$key] = $value;
            }
        }

        return $params;
    }

    public function getParamsDefault($params = [])
    {
        $newParams = $this->keyParams;
        foreach ($params as $key => $value) {
            if (! array_key_exists($key, $this->keyParams)) {
                throw new Exception('The param '.$key.' not exists');
            }
            $newParams[$key] = $value;
        }

        $this->setParams(array_filter($newParams, function ($value) {
            return ! is_null($value);
        }));

        return $this->getParams();
    }

    public function writeExample($path, $args = [])
    {
        $args['example'] = $this->jsonSerialize();
        $tmp = new Template(Template::EXAMPLE, $args);
        file_put_contents($path.'.md', $tmp->getOutput());
    }

    public function jsonSerialize()
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'url' => $this->getURL($this->args),
            'params' => $this->params,
            'status' => $this->status,
            'response' => $this->response,
        ];
    }
}

<?php

namespace Mvochoa\APIDoc;

use Exception;

class Utils
{
    public static function parseComment($comment)
    {
        $pattern = '#(@[a-zA-Z0-9-_]+)((.*(\n\s*\*\s{3,}.*)+)|.*)#';
        preg_match_all($pattern, $comment, $matches, PREG_PATTERN_ORDER);

        $result = [];
        foreach ($matches[1] as $key => $value) {
            $v = preg_replace('/(.*)\n\s*\*\s*(.*)/i', '${1} ${2}', $matches[2][$key]);

            if (array_key_exists($value, $result)) {
                if (! is_array($result[$value])) {
                    $result[$value] = [$result[$value]];
                }
                array_push($result[$value], $v);
                continue;
            }
            $result[$value] = $v;
        }


        return $result;
    }

    public static function parseParamComment($comment)
    {
        $pattern = '#{(.*)}\s+([a-zA-Z0-9-_]+)\s+(.*)#';
        preg_match_all($pattern, $comment, $matches, PREG_PATTERN_ORDER);
        if (count($matches) != 4) {
            throw new Exception('The @param-doc is incorrect. The format is `@param-doc {Type:(String,Number,Enum,Boolean)|required:(optional)|valueDefault}` nameParam Description');
        }
        $args = explode('|', $matches[1][0]);
        $type = trim($args[0]);
        $required = false;
        $value = null;
        if (count($args) > 1) {
            $required = trim($args[1]) == 'required';
        }
        if (count($args) > 2) {
            $value = trim($args[2]);
        }
        $name = trim($matches[2][0]);
        $description = trim($matches[3][0]);

        return new Param($name, $type, $required, $description, $value);
    }
}

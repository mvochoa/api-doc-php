<?php

namespace Mvochoa\APIDoc;

use ReflectionMethod;

class ExampleMethod extends Example
{
    public function __construct($class, $method)
    {
        $rm = new ReflectionMethod($class, $method);
        $comments = Utils::parseComment($rm->getDocComment());

        if (array_key_exists('@title-doc', $comments)) {
            $this->setTitle(trim($comments['@title-doc']));
        }

        if (array_key_exists('@description-doc', $comments)) {
            $this->setDescription(trim($comments['@description-doc']));
        }

        parent::__construct();
        $this->setKey($rm->getName());
    }
}

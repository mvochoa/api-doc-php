# <?php echo $title; ?> / <?php echo $example['title']; ?>

<?php if ($example['description']) { ?>

<?php echo str_replace('\\n', "\n\n", $example['description']); ?>

<?php } ?>

<?php if ($url != $example['url']) { ?>
```
<?php echo $method; ?> <?php echo $example['url']; ?>

```

<?php } ?>
<?php if (count(array_keys($example['params'])) > 0) { ?>
Parámetros enviado:

| Atributo   | Valor  |
| ---------- | ------ |
<?php foreach ($example['params'] as $key => $value) { ?>
| `<?php echo $key; ?>`    | <?php echo $value; ?> |
<?php } ?>

<?php } ?>
Ejemplo de la respuesta (`<?php echo $example['status']; ?>`):

```json
<?php echo json_encode($example['response'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT); ?>

```

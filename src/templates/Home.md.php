<?php foreach ($data as $key => $value) { ?>
- <?php if (array_key_exists($key, $titles)) {
    echo $titles[$key];
} else {
    echo $key;
} ?>

<?php foreach ($value as $k => $v) { ?>
    - [<?php echo $v['title']; ?>](.<?php echo $v['directory'].'Home'; ?><?php echo $link_with_md_extension ? '.md' : '' ?>)
<?php } ?>
<?php } ?>
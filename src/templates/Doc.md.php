# <?php echo $title; ?>


<?php echo str_replace('\\n', "\n", $description); ?>


```
<?php echo $method; ?> <?php echo $url; ?>

```

<?php if (count($params) > 0) { ?>
Parámetros:

| Atributo   | Tipo   | ¿Requerido? | Descripción            | Ejemplo            |
| ---------- | ------ | ----------- | ---------------------- | ------------------ |
<?php foreach ($params as $param) { ?>
| `<?php echo $param->getName(); ?>`    | <?php echo $param->getType(); ?> | <?php echo $param->getRequired() ? 'Si' : 'No'; ?>       | <?php echo str_replace(["\n", "\r"], '', $param->getDescription()); ?>     | <?php echo $param->getValue(); ?> |
<?php } ?>

<?php } ?>
## Ejemplos

<?php foreach ($examples as $key => $value) { ?>
- [<?php echo $value; ?>](./<?php echo $key; ?><?php echo $link_with_md_extension ? '.md' : '' ?>)
<?php } ?>

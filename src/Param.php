<?php

namespace Mvochoa\APIDoc;

use Exception;
use JsonSerializable;

class Param implements JsonSerializable
{
    const ENUM = 'Enum';
    const STRING = 'String';
    const NUMBER = 'Number';
    const BOOLEAN = 'Boolean';
    const FILE = 'File';

    private $name;
    private $type;
    private $required;
    private $description;
    private $value;

    public static function checkType($type)
    {
        if (array_search($type, [self::ENUM, self::STRING, self::NUMBER, self::BOOLEAN, self::FILE]) == -1) {
            throw new Exception('The type is incorrect');
        }
    }

    public function __construct($name, $type, $required = 0, $description = '', $value = null)
    {
        self::checkType($type);
        $this->name = $name;
        $this->required = $required;
        $this->description = $description;
        $this->type = $type;
        $this->value = $value;

        if ($this->value) {
            switch ($this->type) {
                case self::NUMBER:
                    $this->value = floatval($this->value);
                    break;
                case self::BOOLEAN:
                    $this->value = strtolower($this->value) == 'true';
                    break;
                default:
                    break;
            }
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getRequired()
    {
        return $this->required;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'type' => $this->type,
            'required' => $this->required,
            'description' => $this->description,
            'value' => $this->value,
        ];
    }
}
